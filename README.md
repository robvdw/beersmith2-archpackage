# Beersmith Arch Package
Create Arch package from Beersmith 2.3 deb

## Prerequisites
Install **libpng12** from AUR

## Installation (Arch)
* Clone this repo (git clone https://gitlab.com/robvdw/beersmith2-archpackage.git)
* cd beersmith2-archpackage
* makepkg -sri

## Source package
http://beersmith.com/download-beersmith/

## Credits
Based on AUR package beersmith2 by Greg Greenaae <ggreenaae@gmail.com>
